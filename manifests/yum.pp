# == Class: base::yum
#
# Generic YUM configuration for Emerging Technology systems.
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class base::yum(
  $sources,
) {

  validate_hash($sources)

  if $sources {
    create_resources('yumrepo', $sources)
  }
}
