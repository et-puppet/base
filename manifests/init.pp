# == Class: base
#
# Base configuration for Emerging Technology systems.
#
# === Parameters
#
# timezone: defaults to PST8PDT
# locale:   defaults to en_US.UTF-8
#
# === Examples
#
#  include base
#
#  or, for east coast systems
#
#  class { 'base':
#    timezone => 'EST5EDT',
#  }
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class base (
  $timezone,
  $locale,
  $lang,
  $charset,
  $add_cloud_config
) {

  include base::packages
#  include ossec

  if ($add_cloud_config) {

    file { '/etc/cloud/cloud.cfg':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "puppet:///modules/${module_name}/cloud.cfg",
      require => Package['cloud-init'],
    }

  }


  file { '/etc/timezone':
    ensure  => file,
    owner   => 0,
    group   => 0,
    mode    => '0444',
    content => $timezone,
  }

  if ($::osfamily != 'Darwin') {

    file { '/etc/localtime':
      ensure  => link,
      owner   => 0,
      group   => 0,
      mode    => '0644',
      target  => "/usr/share/zoneinfo/${timezone}",
      require => Package['tzdata'],
    }

    file { '/etc/default/locale':
      ensure => file,
      owner  => 0,
      group  => 0,
      mode   => '0444',
      source => "puppet:///modules/${module_name}/etc/default/locale",
    }

  } else {

    file { '/etc/localtime':
      ensure  => link,
      owner   => 0,
      group   => 0,
      mode    => '0644',
      target  => "/usr/share/zoneinfo/${timezone}",
    }

  }

  if ($::osfamily == 'Debian') {
    file { '/etc/locale.gen':
      ensure  => file,
      owner   => 0,
      group   => 0,
      mode    => '0444',
      content => "${locale} ${charset}",
    }

    exec { 'locale-gen':
      command     => '/usr/sbin/locale-gen',
      subscribe   => File['/etc/locale.gen'],
      refreshonly => true,
    }
  }

  if ($::osfamily == 'RedHat') {
    file { '/etc/locale.conf':
      ensure  => file,
      owner   => 0,
      group   => 0,
      mode    => '0444',
      content => "LANG=\"${locale}\"",
    }

    exec { 'locale-def':
      command     => "/usr/bin/localedef -v -c -i ${lang} -f ${charset} ${locale}",
      subscribe   => File['/etc/locale.conf'],
      refreshonly => true,
      returns     => [ 0, 1 ],
    }
  }
}
