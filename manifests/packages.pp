# == Class: base::packages
#
# Installs / updates all the common packages used on Emerging Technology systems
#
# automatically included by base module
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class base::packages (
  $install,
  $uninstall,
  $pip,
  $easy_install,
  $package_type,
  $sources,
){

  include stdlib

  Package { provider => $package_type }

  stage { 'pre-packages':
    before => Stage['main'],
  }

  class { "base::${package_type}":
    stage   => 'pre-packages',
    sources => $sources,
  }

  $only_install = difference($install, $uninstall)

  $installing = join($only_install, ' ')
  $uninstalling = join($uninstall, ' ')

  ensure_packages($only_install, { ensure => present })
  ensure_packages($uninstall, { ensure => absent })

  if ($::osfamily == 'Debian') {
    exec { 'apt-mark':
      command => "/usr/bin/apt-mark manual ${installing}",
      require => Package[$only_install],
    }
  }

  if member($only_install, 'sudo') {
    file { '/etc/sudoers':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0440',
      source  => "puppet:///modules/${module_name}/etc/sudoers",
      require => Package['sudo'],
    }

    file { '/etc/sudoers.d':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0440',
      require => Package['sudo'],
    }
  }

  if (! empty($pip)) {
    ensure_packages(['python-setuptools'], { ensure => present })

    exec { 'easy_install_pip':
      command => "${easy_install} pip",
      require => Package['python-setuptools'],
    }

    # python packages via pip
    ensure_packages ($pip, {
      ensure   => present,
      provider => 'pip',
      require  => Exec['easy_install_pip'],
    })
  }

}
